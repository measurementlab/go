package anonymize_test

import (
	"log"
	"net"
	"testing"

	"github.com/go-test/deep"

	"github.com/m-lab/go/anonymize"
	"github.com/m-lab/go/rtx"
	//"encoding/hex"
)

func verifyNoAnonymization(doesNoAnon anonymize.IPAnonymizer, t *testing.T) {
	ip := net.ParseIP("10.10.4.3")
	doesNoAnon.IP(ip)
	if ip.String() != "10.10.4.3" {
		t.Errorf("anonymizedIP (%s) should be 10.10.4.3", ip.String())
	}

	doesNoAnon.IP(nil) // No crash = success.
}

func TestNoAnon(t *testing.T) {
	verifyNoAnonymization(anonymize.New(anonymize.None), t)
}

func TestBadAnonName(t *testing.T) {
	calls := 0
	revert := anonymize.SetLogFatalf(func(string, ...interface{}) {
		calls++
	})
	defer revert()
	defer func() {
		r := recover()
		if r == nil {
			t.Error("A bad anonymization method should cause a panic, but it did not.")
		}
		if calls == 0 {
			t.Error("calls should not be zero")
		}
	}()
	anonymize.New(anonymize.Method("bad_anon_method"))
}

func TestNetblockAnon(t *testing.T) {
	anon := anonymize.New(anonymize.Netblock)

	anon.IP(nil)                  // No crash = success
	anon.IP(net.IP([]byte{1, 2})) // No crash = success

	anonymize.IgnoredIPs = []net.IP{net.ParseIP("127.0.0.1"), net.ParseIP("1::2")}

	tests := []struct {
		ip   string
		want string
	}{
		{"127.0.0.1", "127.0.0.1"}, // IgnoredIPs should be ignored.
		{"1:0::2", "1::2"},         // IgnoredIPs should be ignored.
		{"10.1.2.3", "10.1.2.0"},
		{"255.255.255.255", "255.255.255.0"},
		{"0:1:2:3:4:5:6:7", "0:1:2:3::"},
		{"aaaa:aaab:aaac:aaad:aaae:aaaf:aaa1:aaa1", "aaaa:aaab:aaac:aaad::"},
	}
	for _, tt := range tests {
		t.Run(tt.ip, func(t *testing.T) {
			ip := net.ParseIP(tt.ip)
			anon.IP(ip)
			if ip.String() != tt.want {
				t.Errorf("netblockAnonymizer.IP() = %q, want %q", ip.String(), tt.want)
			}
		})
	}
}

func TestCryptopAntAnon(t *testing.T) {
	var testKey = []byte{21, 34, 23, 141, 51, 164, 207, 128, 19, 10, 91, 22, 73, 144, 125, 16, 216, 152, 143, 131, 121, 121, 101, 39, 98, 87, 76, 45, 42, 132, 34, 2}
    anonymize.Key = testKey
	anon := anonymize.New(anonymize.CryptoPAn)
	
	anon.IP(nil)                  // No crash = success
	anon.IP(net.IP([]byte{1, 2})) // No crash = success

	anonymize.IgnoredIPs = []net.IP{net.ParseIP("127.0.0.1"), net.ParseIP("1::2")}

	tests := []struct {
		ip   string
		want string
	}{
		{"127.0.0.1", "127.0.0.1"}, // IgnoredIPs should be ignored.
		{"1:0::2", "1::2"},         // IgnoredIPs should be ignored.
		{"128.11.68.132", "135.242.180.0"},
		{"129.118.74.4", "134.136.186.0"},
		{"2001:db8::1", "4401:2bc:603f:d91d::"},
		{"::2", "78ff:f001:9fc0:20df::"},
	}
	for _, tt := range tests {
		t.Run(tt.ip, func(t *testing.T) {
			ip := net.ParseIP(tt.ip)
			t.Logf(ip.String())
			anon.IP(ip)
			if ip.String() != tt.want {
				t.Errorf("CryptoPAnAnonymizer.IP() = %q, want %q", ip.String(), tt.want)
			}
		})
	}
}

func TestMethodFlagMethods(t *testing.T) {
	var m anonymize.Method
	rtx.Must(m.Set("none"), "Could not set to none")
	if diff := deep.Equal(anonymize.None, m.Get()); diff != nil {
		t.Error(diff)
	}
	if diff := deep.Equal("none", m.String()); diff != nil {
		t.Error(diff)
	}
	rtx.Must(m.Set("netblock"), "Could not set to netblock")
	if m.Set("badmethod") == nil {
		t.Error("Should have had an error")
	}
	if diff := deep.Equal(anonymize.Netblock, m.Get()); diff != nil {
		t.Error(diff)
	}
	if diff := deep.Equal("netblock", m.String()); diff != nil {
		t.Error(diff)
	}
	log.Println(m)
}

func Example() {
	ip := net.ParseIP("10.10.4.3")
	anon := anonymize.New(anonymize.IPAnonymizationFlag)
	anon.IP(ip)
	log.Println(ip) // Should be "10.10.4.0" if the --anonymize.ip=netblock command-line flag was passed.
}
